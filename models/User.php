<?php

namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\helpers\ArrayHelper;

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
	
	public $role;
	
	public static function tableName()
	{
		return 'user';
	}

	
	public function rules()
	{
		return
		[
			[['username', 'password', 'authKey'],'string','max'=>255],
			[['username', 'password'], 'required'],
			[['username'], 'unique'],
			['role', 'safe'],
		];
	}
	
	
	
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
	
	{
		return self::findOne($id);
	}
	
 
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('Not supported');

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
		return self::findOne(['username'=>$username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
   //test hashed password

    public function validatePassword($password)
    {
        return $this->isCorrectHash($password, $this->password);;
    }
	
	private function isCorrectHash($plaintext, $hash)
	{
		return Yii::$app->security->validatePassword($plaintext, $hash);
	}
	
	

	//hash password before saving
    public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isAttributeChanged('password'))
            $this->password = Yii::$app->security->
					generatePasswordHash($this->password);

        if ($this->isNewRecord)
		    $this->authKey = Yii::$app->security->generateRandomString(32);

        return $return;
    }

		public static function getUsers()
	{
		$allUsers = self::find()->all();
		$allUsersArray = ArrayHelper::
					map($allUsers, 'id', 'username');
		return $allUsersArray;						
	}

	
}
