<?php

namespace app\models;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "post1".
 *
 * @property integer $id
 * @property string $title
 * @property string $body
 * @property integer $category
 * @property string $auther
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Post1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['body'], 'string'],
            [['category', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['title', 'auther'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'body' => 'Body',
            'category' => 'Category',
            'auther' => 'Auther',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }
	
	public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }	

	public function getUpdateddBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }	
	
	public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }
	
	public function getCategoryItem()
    {
        return $this->hasOne(Category::className(), ['id' => 'category']);
    }
	
	
	public function getUserAuther()
    {
        return $this->hasOne(User::className(), ['id' => 'auther']);
    }
	
	public function behaviors()
    {
		return 
		[
			[
				'class' => BlameableBehavior::className(),
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => 'updated_by',
			 ],
				'timestamp' => [
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
			],
		];
    }
	
	
/*public function beforeSave($insert){		
		$return = parent::beforeSave($insert);
		if ($this->isNewRecord) {
			$this->setAttribute('status','1');
		}

		//5.b
		if($this->isAttributeChanged('status')){
			if (!\Yii::$app->user->can('changeStatus')){
				if($this->getAttribute('status') == 2)
					unset($this->status);
			}
		}
		
		return $return;	
	}*/
}
