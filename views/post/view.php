<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'body:ntext',
            'category',
            'author',
            'status',
			[ // Lead created at
				'label' => $model->attributeLabels()['created_at'],
				'value' => date('d/m/Y H:i:s', $model->created_at)
			],				
            [ // Lead updated at
				'label' => $model->attributeLabels()['updated_at'],
				'value' => date('d/m/Y H:i:s', $model->updated_at)
			],		
            [ // Lead created by
				'label' => $model->attributeLabels()['created_by'],
				'value' => isset($model->created_By->fullname) ? $model->created_By->fullname : 'No one!',	
			],
            [ // Lead updated by
				'label' => $model->attributeLabels()['updated_by'],
				'value' => isset($model->updated_By->fullname) ? $model->updated_By->fullname : 'No one!',	
			],
			
			
           // 'created_at',
           // 'updated_at',
           // 'created_by',
           // 'updated_by',
        ],
    ]) ?>

</div>
