<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Post1 */

$this->title = 'Update Post1: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Post1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="post1-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
