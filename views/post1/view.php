<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\models\Post1 */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Post1s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post1-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'body:ntext',
            //'category',
           // 'auther',
           // 'status',
		   
		   [ // the auther name of the lead
				'label' => $model->attributeLabels()['auther'],
				'format' => 'html',
				'value' => Html::a($model->userAuther->username, 
					['user/view', 'id' => $model->userAuther->id]),	
			],
		   
		   [ // the category name 
				'label' => $model->attributeLabels()['category'],
				'value' => $model->categoryItem->category_name,	
			],
			
			[ // the status name 
				'label' => $model->attributeLabels()['status'],
				'value' => $model->statusItem->status_name,	
			],			
			
			[ // post created at
				'label' => $model->attributeLabels()['created_at'],
				'value' => date('d/m/Y H:i:s', $model->created_at)
			],				
            [ // post updated at
				'label' => $model->attributeLabels()['updated_at'],
				'value' => date('d/m/Y H:i:s', $model->updated_at)
			],		
            [ // post created by
				'label' => $model->attributeLabels()['created_by'],
				'value' => isset($model->createdBy->username) ? $model->createdBy->username : 'No one!',	
			],
            [ // post updated by
				'label' => $model->attributeLabels()['updated_by'],
				'value' => isset($model->updateddBy->username) ? $model->updateddBy->username : 'No one!',	
			],
           // 'created_at',
           // 'updated_at',
           // 'created_by',
           // 'updated_by',
        ],
    ]) ?>

</div>
