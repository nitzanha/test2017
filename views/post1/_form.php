<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Status;
use app\models\Category;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Post1 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="post1-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body')->textarea(['rows' => 6]) ?>


	
	<?= $form->field($model, 'category')->
				dropDownList(Category::getCategories()) ?>


	 <?= $form->field($model, 'auther')->
				dropDownList(User::getUsers()) ?>

	<?php if (\Yii::$app->user->can('changeStatus')) { ?>
    <?= $form->field($model, 'status')->
				dropDownList(Status::getStatuses()) ?>
<?php } ?>

   

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
