<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Post1Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Post1s';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post1-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Post1', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'body:ntext',
            //'category',
            //'auther',
			[
				'attribute' => 'auther',
				'label' => 'Auther',
				'value' => function($model){
					return $model->userAuther->username;
				},
			],
			[
				'attribute' => 'category',
				'label' => 'Category',
				'value' => function($model){
					return $model->categoryItem->category_name;
				},
			],
			
			[
				'attribute' => 'status',
				'label' => 'Status',
				'value' => function($model){
					return $model->statusItem->status_name;
				},
			],
			
            //'status',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
