<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 */
class m170720_062859_create_category_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('category', [
            'id' => 'pk',
            'category_name' => 'string'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('category');
    }
}
