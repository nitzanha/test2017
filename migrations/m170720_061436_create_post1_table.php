<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post1`.
 */
class m170720_061436_create_post1_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('post1', [
             'id' => 'pk',
			'title' => 'string',
			'body' => 'text',
			'category' => 'integer',
			'auther' => 'string',
			'status' => 'integer',
			'created_at' => 'integer',
			'updated_at' => 'integer',
			'created_by' => 'integer',
			'updated_by' => 'integer',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('post1');
    }
}
