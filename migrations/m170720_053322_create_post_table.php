<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post`.
 */
class m170720_053322_create_post_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('post', [
            'id' => $this->primaryKey(),
			'title' => $this->string()->notNull(),
			'body' => $this->text()->notNull(),
			'category' => $this->integer()->notNull(),
			'author' => $this->string()->notNull(),
			'status' => $this->integer()->notNull(),
			'created_at' => $this->integer()->notNull(),
			'updated_at' => $this->integer()->notNull(),
			'created_by' => $this->integer()->notNull(),
			'updated_by' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('post');
    }
}
