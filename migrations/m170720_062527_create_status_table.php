<?php

use yii\db\Migration;

/**
 * Handles the creation of table `status`.
 */
class m170720_062527_create_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('status', [
            'id' => 'pk',
            'status_name' => 'string'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('status');
    }
}
